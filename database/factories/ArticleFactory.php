<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Article>
 */
class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $categoryIds = Category::all()->pluck('id')->toArray();
        return [
            'title' => fake()->sentence(),
            'slug' => fake()->slug(1),
            'content' => fake()->realText(),
            'category_id' => $categoryIds[array_rand($categoryIds)],
        ];
    }
}
