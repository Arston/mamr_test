# mamr_test



## Инструкция по развертыванию

```
1. Запустить docker
2. Запустить команду docker-compose up, дождаться её завершения
3. Убедиться, что в докере запущен проект, и зайти в контейнер докера командой docker-compose exec web bash
4. Проверить версию php: php -v (не ниже 8.1)
4. Запустить команду php composer.phar install
5. Убедиться, что создался файл .env (если нет, сделать копию от .env.example) и сгенерировать ключ php artisan key:generate
6. Проверить доступы к базе данных

DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=laravel_mamr
DB_USERNAME=root
DB_PASSWORD=root

7. Запустить команду php artisan migrate --seed
8. Отдельной вкладкой в консоли установить npm install и запустить npm run dev или npm run build (для авторизации используется laravel breeze, он попросит это сделать)
```

## Используемые пакеты

<pre>
Авторизация - Laravel Breeze
Административная панель - SleepingOwlAdmin (https://sleepingowladmin.ru/#/ru - документация)
</pre>
