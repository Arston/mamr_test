<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if(isset($request->page) && $request->page > 0) {
            //получение статей по параметру page
            $page = $request->page-1;
            $limit = $request->query('limit', 5);
            $data = Article::skip($page * $limit)->take($limit)->get();
        } elseif (isset($request->categoryId) && $request->categoryId > 0) {
            //получение статей по id категории
            $categoryId = $request->categoryId;
            $data = Article::where('category_id', $categoryId)->get();
        } elseif (isset($request->slug) && !is_null($request->slug)) {
            //получение статей по slug
            $slug = $request->slug;
            $data = Article::where('slug', $slug)->first();
        } else {
            //получение всех статей
            $data = Article::all();
        }

        if(!is_null($data)) {
            return $data->toJson();
        } else {
            return response('Статьи не найдены', 404);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = Article::find($id);
        if($data != null) {
            return $data->toJson();
        } else {
            return response('Запись не найдена', 404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
