<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Models\Article;
use SleepingOwl\Admin\Form\FormElements;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Facades\Display;
use SleepingOwl\Admin\Facades\Form;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Section;

/**
 * Class Articles
 *
 * @property \App\Models\Article $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Articles extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(200)->setIcon('fa fa-lightbulb-o');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::link('id', '#')->setWidth('20px')->setHtmlAttribute('class', 'text-center')->setOrderable(function($query, $direction) {
                $query->orderBy('id', $direction);
            }),
            AdminColumn::text('slug', 'Алиас')->setWidth('100px')->setHtmlAttribute('class', 'text-center')->setOrderable(function($query, $direction) {
                $query->orderBy('slug', $direction);
            }),
            AdminColumn::image('image', 'Обложка')->setOrderable(function ($query, $direction) {
                $query->orderBy('image', $direction);
            })->setHtmlAttribute('class', 'text-center'),
            AdminColumn::link('title', 'Название')->setWidth('150px')->setHtmlAttribute('class', 'text-center')->setOrderable(function($query, $direction) {
                $query->orderBy('title', $direction);
            }),
            AdminColumn::text('content', 'Контент')->setWidth('300px')->setHtmlAttribute('class', 'text-center')->setOrderable(function($query, $direction) {
                $query->orderBy('content', $direction);
            }),
            AdminColumn::boolean('active', 'Active')->setOrderable(function($query, $direction) {
                $query->orderBy('active', $direction);
            }),
            AdminColumn::text('updated_at', 'Updated')->setWidth('100px')->setOrderable(function($query, $direction) {
                $query->orderBy('updated_at', $direction);
            }),
            //все столбцы поместить не удается, какая-то ошибка админки, не добавил created_at
            AdminColumn::text('order', 'order')->setWidth('100px')->setOrderable(function($query, $direction) {
                $query->orderBy('order', $direction);
            }),
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[7, 'asc']]) //номер столбца, по которому происходит сортировка см. https://datatables.net/examples/basic_init/table_sorting.html
            ->setDisplaySearch(true)
            ->paginate(25)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        $display->setColumnFilters([
            AdminColumnFilter::select()
                ->setPlaceholder('Все категории')
                ->setColumnName('category_id')
                ->setModelForOptions(Category::class, 'title')
                ->setLoadOptionsQueryPreparer(function($element, $query) {
                    return $query->select('id', 'title')->whereHas('articles')->orderBy('id', 'ASC');
                })->setSortable('true'),
        ]);
        $display->getColumnFilters()->setPlacement('card.heading');

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $form = new FormElements([
            AdminFormElement::image('image', 'Обложка'),
            AdminFormElement::text('title', 'Название')->required(),
            AdminFormElement::text('slug', 'Алиас')->required(),
            AdminFormElement::select('category_id', 'Категория')->setModelForOptions(Category::class, 'title')->required()
                ->setLoadOptionsQueryPreparer(function ($item, $query) {
                    return $query->where('active', 1);
                })->required(),
            AdminFormElement::ckeditor('content', 'Текст')->required(),
            AdminFormElement::checkbox('active', 'Активность')->setDefaultValue(1),
            AdminFormElement::text('order', 'Порядок сортировки')
        ]);

        $tabs = Display::tabbed();
        $tabs->appendTab($form, 'Основное')->setIcon('<i class="fa fa-file-alt"></i>');

        $panel = Form::panel();
        $panel->addElement($tabs);

        return $panel;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
