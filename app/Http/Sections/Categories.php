<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Facades\Display;
use SleepingOwl\Admin\Facades\Form;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Section;

/**
 * Class Categories
 *
 * @property \App\Models\Category $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Categories extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(150)->setIcon('fa fa-lightbulb-o');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::link('id', '#')->setWidth('20px')->setHtmlAttribute('class', 'text-center')->setOrderable(function($query, $direction) {
                $query->orderBy('id', $direction);
            }),
            AdminColumn::link('title', 'Название')->setWidth('500px')->setHtmlAttribute('class', 'text-center')->setOrderable(function($query, $direction) {
                $query->orderBy('title', $direction);
            }),
            AdminColumn::boolean('active', 'Active')->setWidth('100px')->setOrderable(function($query, $direction) {
                $query->orderBy('active', $direction);
            }),
            AdminColumn::text('created_at', 'Created')->setWidth('150px')->setOrderable(function($query, $direction) {
                $query->orderBy('created_at', $direction);
            }),
            AdminColumn::text('updated_at', 'Updated')->setWidth('150px')->setOrderable(function($query, $direction) {
                $query->orderBy('updated_at', $direction);
            }),
            AdminColumn::text('order', 'order')->setWidth('100px')->setOrderable(function($query, $direction) {
                $query->orderBy('order', $direction);
            }),
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[5, 'asc']])
            ->setDisplaySearch(true)
            ->paginate(25)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        $display->setColumnFilters([
            AdminColumnFilter::select()
                ->setModelForOptions(\App\Models\Category::class, 'name')
                ->setLoadOptionsQueryPreparer(function($element, $query) {
                    return $query;
                })
                ->setDisplay('name')
                ->setColumnName('name')
                ->setPlaceholder('All names')
            ,
        ]);
        $display->getColumnFilters()->setPlacement('card.heading');

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $form = new FormElements([
            AdminFormElement::text('title', 'Название')->required(),
            AdminFormElement::checkbox('active', 'Активность')->setDefaultValue(1),
            AdminFormElement::text('order', 'Порядок сортировки')
        ]);

        $tabs = Display::tabbed();
        $tabs->appendTab($form, 'Основное')->setIcon('<i class="fa fa-file-alt"></i>');

        $panel = Form::panel();
        $panel->addElement($tabs);

        return $panel;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
